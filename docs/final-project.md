# Projet final

![](./images/miseenpageGIT1.jpg)
Plus d’infos sur [wikipédia](https://fr.wikipedia.org/wiki/Pok%C3%A9mon)

![](./images/miseenpageGIT1bis.jpg)
![](./images/miseenpageGIT2.jpg)
![](./images/miseenpageGIT3.jpg)

Source : « Lire et comprendre une carte Pokémon”, sur le site Pokémon carte, [pokemoncarte](https://pokemoncarte.com/lire-et-comprendre-carte-pokemon-15) (site consulté le 31 septembre 2021)
![](./images/miseenpageGIT3bis.jpg)
![](./images/miseenpageGIT4.jpg)

## Séléction
![](./images/miseenpageGIT5.jpg)
![](./images/miseenpageGIT6.jpg)
![](./images/miseenpageGIT7.jpg)
![](./images/miseenpageGIT8.jpg)
![](./images/miseenpageGIT9.jpg)

[Wikicarte](https://fr.m.wikipedia.org/wiki/Protège-carte)

## Contexte
![](./images/miseenpageGIT9bis.jpg)
![](./images/miseenpageGIT10.jpg)
![](./images/miseenpageGIT11.jpg)
![](./images/miseenpageGIT12.jpg)

# Porte-cartes impression 3D
## P.C. V1
![](./images/miseenpageGIT13.jpg)
![](./images/miseenpageGIT14.jpg)
## P.C. V2
![](./images/miseenpageGIT15.jpg)
![](./images/miseenpageGIT16.jpg)
## P.C. V3
![](./images/miseenpageGIT17.jpg)
## P.C. V4
![](./images/miseenpageGIT18.jpg)
# Porte carte plexiglas
## P.C. V5
![](./images/miseenpageGIT19.jpg)
![](./images/miseenpageGIT20.jpg)
![](./images/miseenpageGIT21.jpg)
![](./images/miseenpageGIT22.jpg)
![](./images/miseenpageGIT23.jpg)
![](./images/miseenpageGIT24.jpg)
## Test des cartes au musée
![](./images/miseenpageGIT25.jpg)
![](./images/miseenpageGIT26.jpg)
## Séléction 2
![](./images/miseenpageGIT27.jpg)
![](./images/miseenpageGIT28.jpg)
![](./images/miseenpageGIT29.jpg)
![](./images/miseenpageGIT30.jpg)
![](./images/miseenpageGIT31.jpg)
![](./images/miseenpageGIT32.jpg)
## Design de la carte
![](./images/miseenpageGIT33.jpg)
![](./images/miseenpageGIT34.jpg)
![](./images/miseenpageGIT35.jpg)
## P.C. V6
![](./images/miseenpageGIT36.jpg)
![](./images/miseenpageGIT37.jpg)
# Boite à badge
## B.à.b. V1
![](./images/miseenpageGIT38.jpg)
![](./images/miseenpageGIT39.jpg)
![](./images/miseenpageGIT40.jpg)
# Récompense
![](./images/miseenpageGIT41.jpg)
![](./images/miseenpageGIT42.jpg)
![](./images/miseenpageGIT43.jpg)
## B.à.b. V2
![](./images/miseenpageGIT44.jpg)
![](./images/miseenpageGIT45.jpg)
![](./images/miseenpageGIT46.jpg)
![](./images/miseenpageGIT47.jpg)
![](./images/miseenpageGIT48.jpg)

Sur les différents couvercles j'ai gravé les informations sur le plastique qui compose l'objet. On y retrouve : l'année de création du plastique, un autre objet de la vie quotidienne composé avec celui-ci et ses propriétés mécaniques.
Site consulté pour récolter les informations :
- [nelinkia](https://www.nelinkia.com/blog/applications/defintion-polyethylene-utilisation-plaque-polyethylene.html#:~:text=Les%20avantages%20du%20poly%C3%A9thyl%C3%A8ne,veut%20aussi%20robuste%20et%20inalt%C3%A9rable)
- [roto30-rotomoulage](https://roto30-rotomoulage.com/polyethylene-caract%C3%A9ristiques-avantages-inconv%C3%A9nients.html)
- [artis-groupe](https://www.artis-groupe.fr/blog/polyethylene-inconvenients)
- [aristegui](https://www.aristegui.info/fr/utilisations-et-avantages-du-hdpe/)
- [builddaysis](https://builddaysis.com/fr/issues/8466)
- [rotomoulage](https://www.rotomoulage.org/fr/le-rotomoulage/18-caracteristiques/43-matieres-plastiques.html)
- [plastisem](https://www.plastisem.fr/guide-plastique/les-matieres-plastiques/)
- [flexpipeinc](https://www.flexpipeinc.com/ca_fr/technical_data/comparaison-des-revetements-plastiques-pe-vs-abs-vs-pvc-vs-pp/)
- [simplyscience](https://www.simplyscience.ch/fr/enfants/decouvre/du-plastique-mais-pas-nimporte-lequel?fbclid=IwAR21Tnay-N8McZ-cJ4lfsunLCxvnctvVKg-TlONFQkOEawSOAXXN9jF1rS4   )
- [polyalto](https://blogue.polyalto.com/abs )
- [paprec](https://www.paprec.com/fr/comprendre-le-recyclage/tout-savoir-sur-les-matieres-recyclables/plastiques/la-classification-des-types-de-matieres-plastiques/)
- [wikipedia](https://fr.wikipedia.org/wiki/Polystyr%C3%A8ne)
- [snpu](https://snpu.fr/actualites/cest-quoi-le-polyurethane)

![](./images/miseenpageGIT49.jpg)
![](./images/miseenpageGIT50.jpg)
![](./images/miseenpageGIT51.jpg)

## B.à.b. V3
![](./images/miseenpageGIT52.jpg)
![](./images/miseenpageGIT53.jpg)

# Système portatif
![](./images/miseenpageGIT54.jpg)
# Production crash test
![](./images/miseenpageGIT55.jpg)
![](./images/miseenpageGIT56.jpg)

# CRASH TEST
![](./images/miseenpageGIT57.jpg)
![](./images/miseenpageGIT58.jpg)
![](./images/miseenpageGIT59.jpg)
![](./images/miseenpageGIT60.jpg)
![](./images/miseenpageGIT61.jpg)

# Essai texture
![](./images/miseenpageGIT62.jpg)
![](./images/miseenpageGIT63.jpg)

## P.C. V6
![](./images/miseenpageGIT64.jpg)
![](./images/miseenpageGIT65.jpg)

## P.C. V7
![](./images/miseenpageGIT66.jpg)
![](./images/miseenpageGIT67.jpg)

## B.à.b. V4
![](./images/miseenpageGIT68.jpg)
![](./images/miseenpageGIT69.jpg)
![](./images/miseenpageGIT70.jpg)

Source :  « Comment Fonctionne une Serrure à Code”, sur la plateforme Youtube,  https://www.youtube.com/watch?v=Exz40Tes93k&ab_channel=SYMPA (site consulté le 5 décembre)

![](./images/miseenpageGIT70bis.jpg)
![](./images/miseenpageGIT71.jpg)
![](./images/miseenpageGIT72.jpg)
![](./images/miseenpageGIT73.jpg)
![](./images/miseenpageGIT74.jpg)
![](./images/miseenpageGIT75.jpg)

## B.à.b. V4.1
![](./images/miseenpageGIT76.jpg)
![](./images/miseenpageGIT76bis.JPG)

J’ai fait des variantes de boites en ajoutant les questions et réponses. Je dois les faire tester par d’autres personnes pour voir si le fonctionnement est intuitif ou non.
![](./images/miseenpageGIT76.1bis.JPG)

# Formation brother innovis
![](./images/miseenpageGIT77.jpg)
![](./images/miseenpageGIT78.jpg)
![](./images/miseenpageGIT79.jpg)
![](./images/miseenpageGIT80.jpg)
![](./images/miseenpageGIT81.jpg)
![](./images/miseenpageGIT82.jpg)
![](./images/miseenpageGIT83.jpg)
# Confection du tour de cou
![](./images/miseenpageGIT84.jpg)
![](./images/miseenpageGIT85.jpg)
![](./images/miseenpageGIT86.jpg)
![](./images/miseenpageGIT87.jpg)
![](./images/miseenpageGIT88.JPG)
![](./images/miseenpageGIT89.JPG)

L'idée est de reprendre la toile en polypropylène puis de la découper en bande pour y broder le nom du musée "BRUSSELS DESIGN MUSEUM". Ensuite nous allons le rattacher aux mousquetons qui s'accrochera au collecteur pour pouvoir le porter autour de son cou.

## P.C. V8
![](./images/miseenpageGIT90.JPG)

Lien [Youtube](https://www.youtube.com/watch?v=WFXieuIcPdU&ab_channel=Celal%C3%9Cnal) d'une vidéo qui reprend les différents assemblages en bois.
![](./images/miseenpageGIT91.JPG)
![](./images/miseenpageGIT92.JPG)

# After pré-jury
![](./images/1.JPG)

Pour être plus efficace nous nous sommes répartis le travail.

Farah a poursuivi la fabrication du collecteur en intégrant le procédé d'impression 3D.
Lien vers son GIT Farah

Arno s'est occupé de la confection des bandoulièresen utilisant la machine à coudre brother innovis et la vérification des cartes + gravure du logo.

Je me suis occupé des modifications pour la boite à badge ainsi que les badges.

## B.à.b.V3.1
![](./images/2.JPG)
![](./images/miseenpageGIT94.JPG)

## B.à.b. V4.2
![](./images/miseenpageGIT95.JPG)
![](./images/miseenpageGIT96.JPG)

# Intégration des B.à.b dans le musée
![](./images/miseenpageGIT97.JPG)

[ROTOR](https://rotordc.com/)
![](./images/miseenpageGIT98.JPG)
![](./images/miseenpageGIT99.JPG)
![](./images/miseenpageGIT100.JPG)

# Finalisation des cartes
![](./images/miseenpageGIT101.JPG)
![](./images/miseenpageGIT102.JPG)

# Vidéo d'intro au jeu
![](./images/miseenpageGIT104.JPG)

**Lien vers la vidéo youtube :** [DESIGN MUSEUM BRUSSELS : "MASTER COLLECTOR"](https://www.youtube.com/watch?v=y27lVl0zP30&ab_channel=tjalbia)


Le montage de ce court métrage a été réalisé avec le logiciel Premiere Pro.
Voici les tutos qui m’ont aidé dans le montage :

[VHS Effect](https://www.youtube.com/watch?v=ly9QpaAEqMw&t=204s&ab_channel=KellanReck)

[Wall Transition](https://www.youtube.com/watch?v=esscZeSI0RI&ab_channel=BrookerFilms)

[SMOOTH ZOOM](https://www.youtube.com/watch?v=PoNwGI26QkU&ab_channel=Derri%C3%A8reLaCam%C3%A9ra)
