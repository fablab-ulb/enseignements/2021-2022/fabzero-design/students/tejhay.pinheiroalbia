# Semaine 3   

## Dossier pédagogique  

Après avoir épluché des dossiers pédagogiques et passez en revue les avantages et les inconvénients de chacun, nous nous sommes penchés sur son utilité.  

Le dossier pédagogique sert à aider le professeur à préparer sa visite.  Celui-ci prend connaissance du contenu du musée.  

L’objet sera une manière de transmettre la matière. Pour cela, il faudrait peut-être penser à quelque chose en amont au niveau pédagogique en parallèle à l’objet. Il faut briefer le professeur sur le musée car souvent ils ne connaissent rien sur son contenu.   

Scénarisation d’une ligne du temps qu’est-ce qu’il se passe :
Prof contact le musée -> musée fournit le mode d’emploi de l’objet + dossier pédagogique -> explication sur la visite et l’utilisation de l’objet -> La classe arrive au musée -> les cartes sont fournit -> commencement de la visite et rappel de l’emploie de l’objet -> visite en cours + utilisation de l’objet.
Socle de compétence de l’enseignement, qu’est-ce que doit apprendre l’élève et a quel moment.   

 Savoir, savoir être et savoir faire
Le dossier pédagogique est accessible sur le site du musée.  


## Visite complémentaire  

Ces échanges se sont poursuivis par une visite donnée par Christina en complément de celle de Terry.
Elle nous à donner ses explications sur les objets du musée. Christina a également expliqué les  différents changements qui seront apporté dans le musée dans les années à venir.  

Au début, il y avait assez peu d’objets, c’étaient les in contemporains, de fins des années 70 jusque aujourd’hui. Il y a beaucoup de prêt auprès des institutions qui se termine donc ils doivent renvoyer les objets. Un nouveau projet pour redéfinir l’espace et le parcours va se mettre en place dans les mois à venir pour donner un peu plus de sens. Pour l’instant c’est à moitié chronologique à moitié thématique. L’idée est de revoir ça sans changer les espaces en apportant des grandes thématiques pour voir le design, le cycle de production et le système pas juste l’objet en soi.
Les espaces seront réorganisés en salles :   

-	Production de masse (problématique)
-	L’ergonomie et le rapport au corps
-	Space age
-	Innovation/ recherche technique
-	Année 50-60  

1er pièce
La chaise de Joe Columbo.
Cette chaise qui est la première à être récupéré, c’est un bon exemple de design industriel pour montrer tous les aspects au niveau technique. C’est la première chaise à être injecté en une seule pièce et en ABS.  Le moule de la chaise a été dessiné ainsi pour pouvoir empiler les chaises et aussi pour le transport ce qui est important à considérer dans une production industrielle avec des grands volumes, il y a le moule etc mais il y a les coûts de transport, d’emballage de stockage. Quand on dessine le moule on lui donne la forme qu’on veut. On a fait également tout un travail d’adaptation avec les techniciens qui connaissent la matière.
Le dessin est un peu différent, par exemple il y a des éléments sur le côté ou des détails formels qui sont pas du tout esthétiques mais qui sont dus au moule. Quand on va sortir la chaise du moule il faut avoir assez d’espace pour pouvoir repousser la chaise. Même chose pour les deux trous, il y a un  bras  qui pousse et qui sort la chaise. La grande différence par rapport aux autres éditions c’est qu’on pouvait enlever et changer les pieds pour changer la hauteur parce que la chaise avait été pensée comme chaise de bureau, pour enfants, chaise de bar, etc. Après les habitudes ont changé donc maintenant la chaise est moulé en une seule pièce. Donc les pieds ne se détachent plus. Au niveau des éléments, le marketing et les études qu’ils ont faits montraient que c’était plus intéressant d’avoir différentes typologies.  


On sait qu’après la deuxième guerre mondiale, on est dans un pays dévasté par la guerre et il n’y avait plus d’argent pour le logement donc on est dans un pays où on reconstruit tout et le plastique va permettre à tout le monde d’acheter des biens primaires et pas seulement acheter une chaise mais pouvoir aussi choisir la couleur et la forme. C’est un objet très industriel, construit par la machine.   


(Question) Comment un designer arrive à imaginer à partir de la même base une chaise haute, une table etc ?  

Avec l’espace, la nécessité d’avoir et pas assez de moyen, c’est plus facile et intelligent d’avoir juste deux pièces et de changer juste les pieds plutôt qu’avoir 4 chaises différentes pour avoir des chaises qu’on peut utiliser comme salle à manger puis changer les pieds et on peut l’utiliser dans un autre endroit. Donc dans les années 50-60 le designer travaille sur cette idée de modernité, de standardisation, car il faut absolument qu’un objet soit le plus standard possible pour faciliter la production, c’est un peu le même système qu’aujourd’hui que « Urban Structure » qui utilisent des éléments standards pour permettre à tout le monde de pouvoir le faire parce que par exemple la vis M5 c’est la même pour tout le monde, c’est standard.  


### Salle 1   

Lampe de Galanti qui a un cylindre avec la partie du dessus qui peut être utilisée comme une lampe du genre abat-jour et peut être montée sur sa base, ainsi on peut s’adapter à notre environnement, à notre envie, et ça permet de pouvoir reconstruire. C’est aussi le même système pour la lampe Joe Colombo on change la base ou le fauteuil « half and half  » c’est deux morceaux pareils c’est le même moule mais tu peux le transporter démonter. Il dessine le module double et après il y a plein d’accessoires, par exemple il y a des accroches métalliques pour pouvoir mettre des pots à fleur à l’intérieur ou pour accrocher une ampoule. Donc il crée une base puis il crée des accessoires pour pouvoir un peu « hacker » l’objet. Avoir un objet de production de masse mais donner à l’utilisateur de la possibilité de l’agencer comme il veut et d’avoir du « pouvoir » sur l’achat.  


Même pour les chaises Hills . Le designer crée un système avec des chaises avec acronymes différents. Quand on est designer il faut dessiner mais il faut réfléchir à l’utilisateur et il faut absolument vendre. Donc il crée un système de 7 chaises qui ont le nom d’un acronyme différent sur base de ses composants (ex : dining, side, wire, etc.). Il y a des pieds en bois croisés, en métal, des couleurs différentes et c’est l’utilisateur qui choisit toutes les parties de l’objet c’est vraiment le principe de la personnalisation. Maintenant on peut choisir la couleur, la texture, vraiment dans le détail, ça c’est au tout début, mais ça avait déjà commencé avant avec Peter Perens  avec ses bouillards. Il y en avait trois et ils avaient un catalogue et la personnelle qui achetait disait « ok celle-là je la veux carrée, circulaire, doré, argent, etc » donc c’est cette idée de faire sentir à l’utilisateur comme faisant partie du procédé de création, maintenant on le voit très bien avec tout ce qui est prototypassions opensource.   

Donc dans les années 50-60 il y a tous ces procédés et aussi l’image du design comme profession qui arrive dans la vraie vie, qui pose devant leur création, c’est quelque chose qui n’existait pas avant et surtout avant c’étaient les architectes qui dessinaient les objets et petit à petit on voit des figures qui ce seront des designers.   


Dans les expos ce sont des petits meubles circulaires avec les petites portes qui sont les premiers injectés en ABS. Ce sont des petits meubles rangement qu’on peut empiler. Aujourd’hui il y a des modules A2 A3 A4, avant tous les petits modules pouvaient être empilés et ils avaient dessinés plein d’accessoires. Ils le vendaient en disant que c’était pour tout le monde et c’est pour tous les environnements, on peut utiliser dans les cuisines (plateau en bois), salle de bain (accroche serviette etc) donc c’était vraiment toute l’étude des comportements de la vie de tous les jours et essayer de rentrer avec des objets très spécifiques et fonctionnels mais qui pouvaient être un peu adaptés avec la personne qui l’utilisait. Et surtout c’est tout nouveau, parce qu’avant les gens n’avaient pas cette conscience et le rapport au plastique qu’avant c’était cheap. Donc ils ont dû créer tout un univers culturel qu’ils ont dû changer complètement.   

### Vitrine 1er salle   

Plutôt des formes géométriques, on ne voit pas très souvent de l’organique parce que c’est toute la période rationaliste donc on a des couleurs primaires aussi, ça reste encore surtout des architectes. La machine à écrire Valentine qui a du succès de Olivetti une entreprise de lumière super importante dans le design italien en général, car c’est une des premières qui analyse le design comme design total, donc ils ne dessinent pas que les objets, communication etc mais surtout Olivetti a créé tout un village pour ses ouvriers et une école pour leurs enfants donc il a vraiment créé un système total où ils développent tous ces projets dans un univers d’Olivetti. Le directeur créatif et il crée la Valentine qui est très connue parce que c’est la première. Ce qui est intéressant outre la couleur c’est le fait que ce soit une machine à écrire colorée en ABS et aluminium donc très légère contrairement à avant où elle était très volumineuse. Cela s’inscrit complétement dans la dynamique dans années 60 où il y a eu des décisions politiques qui ont donné plus de temps libre aux travailleurs pour pousser les achats et les loisirs (triennale de 68 sur le loisir) donc toutes les pubs à l’époque était tournée vers ça. Il y a l’idée de la secrétaire qui n’est plus une femme collée à une table mais qui prend sa machine où elle veut pour écrire donc c’est vraiment une machine à écrire portable en plastique avec la protection encastrée par d’ajout c’est plus un grand boitier c’est juste un morceau qui rentre et s’encastre et que je peux amener où je veux parce qu’elle est légère. Thème de la mobilité et du portatif dans la fin des années 60, avec plein d’objets comme le projecteur ou la télé de Roger Tallon. Qu’on peut amener où on veut. Nouvelle thématique de reprise économique qui amène à bouger et à créer plein d’objets qu’on peut déplacer.  


Après dans tout ce qui est typologie d’objet, très important c’est aussi dans ces années-là le design graphique par exemple la orange de Gino Vale, le design graphique a été fait par Lella Vigneli et en fait ils étaient frère. Attention à tous ces petits éléments graphiques.  

Autre histoire, le téléphone de … ça a été une icône parce que c’est le premier où on a le micro et tous les éléments intégrés dans une seule pièce, il n’y a plus de câble, on n’est plus obligé d’avoir le téléphone fixe.  


### Salle 2  

Thématique porte à faux :
L’exemple typique c’est la « Panton chair » et c’est le père du ciment actuel de Vitra qui décide de prendre le challenge et de la produire. La première version est en polymère et elle est éditée en genre 500 exemplaires, mais elle n’est pas optimale et elle coûte trop chère pour la production industrielle. Il continue à la retravailler pour arriver à la deuxième version en ASA et donc elle était trop lourde ils voulaient la rendre plus légère mais si on enlevait l’épaisseur ça ne tenait plus. Donc ils changent le matériau avec de meilleurs propriétés et ce qui va changer c’est qu’il y a des nervures en dessous (c’est ce qui nous permet de reconnaître l’édition). En-dessous il y a un renfort dans cette partie qui est la plus fragile, ça permet aussi de retravailler la forme légèrement, l’épaisseur change dans cette partie donc elle est plus légère. Après la production s’arrête pendant une dizaine d’années, et quelques années avant sa mort Panton et Vitra la retravaillera et fera une troisième édition en polypropylène où elle est beaucoup plus légère, la forme change et l’épaisseur aussi. Les formes sont plus organiques et on arrive à enlever les nervures qui étaient un détail structure et ici il y a le point d’injection, petit cercle. Puis il y a une quatrième édition qui arrive sur le marché en 2020 et c’est le même matériau mais ils ont changé l’assis parce qu’ils ont fait des études de marketing et ils ont vu que les tables actuelles sont légèrement plus hautes donc la chaise d’avant était trop basse donc assis montée de 4cm. Ça c’est la dernière version avec les nouvelles couleurs car en fonction des années les couleurs changent, on a des couleurs ne pastelles pas que du jaune/orange/rouge comme avant, car il y a des couleurs plus compliquées à obtenir comme le bleu, le mauve et le noir. Dans la première partie on n’a pas souvent d’objets qui ont ces couleurs.  


La chaise de Morrison là aussi c’est toujours cette idée de la  chaise la plus légère, la moins épaisse. Là c’est la première faite en un seule moule / une seule pièce qui répond à la forme de la Mark Stam. Parce que déjà …. avait essayé avec ces formes années 2000, mais voilà c’est quelque chose d’un peu plus technique on voit que c’est pas la même matière, là c’est un matériaux super high tech en fibre de verre.   


### Salle 3  

Cet espace deviendra l’espace ergonomie. Et ce sera une partie où on aborde la thématique d’investir l’espace complet, la discothèque sera remise, les bureaux remplacés par tous les objets de l’expo Visionna que l’on voit dans la vidéo.
Le projet Visionna organisé par Bayern entre 1969 et 71 chaque année pour la foire de Cologne ils louaient un bateau et ils ont demandé à des designers d’investir le bateau et de créer des environnements complets donc il y a eu Panton, invité deux fois, Une fois Joe Colombo et une fois Olivier Mourgue. Qui ont vraiment créer tout un espace (voir vidéo du musée) il y a des lumières, des accessoires au mur, la table lumineuse, etc.  

C’était vraiment l’idée de faire un environnement tout en plastique et il y avait la construction en mousse pour montrer comment les investissements et les innovations avec le plastique. Dondolo c’est une prouesse technique car il est produit par extrusion ça a été possible parce que c’est de la fibre de verre renforcée.  


### Podium PMMA  

Pour le transport c’est toute une histoire parce que ce que les designers recherchait c’est d’avoir une seule pièce ne pas montrer le point d’injection et d’avoir une surface avec de bonne propriété optique. Le fauteuil ça c’est un exemple parce que c’est un canapé qui fait 1m90 moulé d’une seule pièce. Donc le moule pèse 30 tonnes et le plastique a été injecté au même moment. En fait il faut imaginer le canapé renversé et il y avait des morceaux qui permettaient d’extraire le canapé parce que l’épaisseur de presque 1cm donc une grande quantité de plastique ! C’est un canapé qui pèse 30 kilos.  

Il y a une recherche de texture maintenant qu’ils ont réussi à avoir de bonnes propriétés optiques. Ils travaillent sur la création de moules qui permettent d’avoir des jeux d’épaisseur et ils arrivent même à avoir des vides à l’intérieur pour pouvoir déformer le plastique et obtenir des effets de faux cristal. Ils ont fait de nouveaux vases et verres avec plein de texture et c’est juste le plastique gonflé à certains endroits qui donne ces effets. La texture devient structurelle donc il y a plus besoin d’ajouter des procédés après, tandis qu’avant il fallait créer et retravailler.  


### Podium gonflable  

Ce sont des films plastiques qui sont soudés entre eux Quasar Khanh le faisait artisanalement lui-même, il n’y a pas grand-chose mais ça doit être un procédé qui est vraiment imaginé à partir du film comment plier et souder pour pouvoir avoir la forme finale.   


### Radical Design  

Après dans tous ce qui est période radicale (fin 60 – début 70) il y a tous ces grands mouvements de contestation contre le système industriel et capitaliste, donc tout ce qui est ce genre de mousse c’est deux entreprises Gufram et Poltronova qui produisent en Italie édition limité 200 exemplaires par exemple le cactus où le moule avec une partie en mousse où tous les bras sont ajoutés sont ajoutés, il y a des parties métalliques qui sont encadrées et tous les détails sont fait à la maison par un monsieur avec un petit outil il fait toute la finition et aussi le vernis (aérographe) le vernis s’appelle Guflac a base de caoutchouc créé par Gufram, revêtement très brillant tout est recouvert avec ce vernis. Donc cette période c’est petite quantité et tout ce qui est artisanal et souvent fait par les designers eux-mêmes.   


### Podium art  

La chaise Pratt Gaetano Pesceà new-york il travaillait surtout avec la résine et il veut exploiter au max ce matériau donc il laisse couler la matière et prendre sa forme. Il créait un moule pour les chaises pour que la forme soit la même et il laissait les ouvriers choisir le mélange, donc les chaises sont uniques puisque les ouvriers décident les couleurs, la proportion de matériaux etc.
Et pour la Pratt chair, il a créé 9 prototypes de chaises où il changeait la quantité de plastique et donc le premier prototype qui n’était même pas solide et était petit à petit il modifiait la quantité de polyuréthane pour avoir une chaise de plus en plus structurelle et confortable, la 8ème est la plus réussie, le prototype 9 devient trop rigide au toucher la texture est bizarre.  


### Podium minimaliste  

On a tout ce qui est impression 3D, le fait de pouvoir avoir des formes pareilles ou des objets d’une seule pièce, ça devient technologique. Fauteuil de Philippe Starck par exemple Ça s’est fait par « thermomoulage » c’est une seule pièce, il y a un fauteuil et un canapé.   


Dans les années 80, après radical design se crée Memphis un studio qui dure quelque années où ils veulent mettre dans les objets toutes les idées de kitch, références exotiques et ils introduisent la … en plastique où on applique les textures par exemple une entreprise … qui devient l’entreprise de ce mouvement et on a tous les objets Memphis avec les couleurs brillantes, néons, fluos, qui reflètent la globalisation qui arrive, le premier mouvement Memphis on a des designers internationaux donc c’est vraiment dans tout le monde, il y a Michael Graves aux USA, Kura Mata au Japon, il y a beaucoup d’italiens, français, anglais, il n’y a plus de barrières donc on commence à travailler sur une échelle globale avec des moyens de communication, de la pub, beaucoup d’images, etc donc ça c’est le contexte post-radicale, mais en 1971-73 crise pétrolière donc le plastique était encore plus mal considéré donc il y a des entreprises comme Kartell ils doivent réinventer leur image et leurs propos donc dans les années 80 il y a tout un phénomène qu’on appelle internationalisation où les entreprises pour se relancer vont chercher des designers internationaux et c’est le moment où arrivent Philippe Starck, Tom Dickson arrivent donc les grandes entreprises traditionnelles italiennes vont chercher des designers à l’étranger et il y a côté de ça tout un changement de style, de couleurs pastelles, de formes organiques et sinueuses, il ne faut pas faire quelques chose de fonctionnel mais d’émotionnel au toucher, travailler sur la texture etc, il y a toutes des nouvelles couleurs et formes.  

C’est encore ce qui se passe encore un peu aujourd’hui et qui continue encore aujourd’hui parce qu’il y a Kartell qui demande à Nendo de faire le cheval à bascule et Alessi qui avant faisait que de la fonderie et dans les années 90 il fait toute une nouvelle série en plastique avec des petits objets, ils font des workshops pour aller plus loin et ajouter le coté communicatif de l’objet qui est important, comme si on avait des compagnons du quotidien. Ce qui joue beaucoup aujourd’hui c’est toute la question du marketing et de la communication, de recyclage mais aussi comment intégrer l’utilisateur dans le procédé. On voit de plus en plus l’objet disparaître pour créer des systèmes de collaboration où on implique des producteurs et des matériaux locaux.
