# Semaine 1   

# ![](../images/ADAM_logo.png)  
 
## Présentation du musée par Christina et Terry  

Le musée du Design de Bruxelles situé près de l’Atomium, accueille la collection plastique depuis 2015.
L’expo permanente s’appelle **« plastic design collection »** reprenant une collection de design en plastique datant des années 50 jusqu’à aujourd’hui. Depuis 2020, Une deuxième collection est axée principalement sur le design belge, ** « Belgisch Design Belge »**, en collaboration avec la **fondation roi Baudouin** (pas que du plastique). 
C’est également un lieu de recherche technique des matériaux et les domaines plus techniques comme sportif et médical. Les questions de l’impact du design sur la société et le quotidien sont également explorés. C’est comme un laboratoire où les expérimentations sont possibles grâce à des collaborations avec la fondation roi Baudouin par exemple qui s’occupe de financer une partie de la collection. Ils mettent en dépôt des pièces de design belges qui ne sont (pas toutes en plastiques) obtenues dans des ventes aux enchères. Les objets sont mis **en dépôt** au musée c’est-à-dire que les pièces appartiennent à la fondation roi Baudouin et ne font pas parties de la collection du musée mais peuvent être exposés au musée. Autre exemple certains objets de la collection Wallonie Bruxelles sont repris pour être exposé dans des musées.
On retrouve un autre musée du **Design en Belgique à Gand** qui n’est plus en relation avec celui de Bruxelles. A Bruxelles le musée a un projet de recherche développé sur le plastique.  La spécificité de Gand est qu’il est plus axé sur le **design belge expérimental, art déco et contemporain** avec des designers flamands. On retrouve une collection plus vaste et moins spécifique du design
Au **ADAM Museum à Bruxelles**, le but est de parler de l’histoire du **design et ses origines**. On retrouve surtout du design industriel.
Pour cela le musée propose une exposition permanente et des expositions temporaires qui sont accessible à travers des visites guidées ou libre, des workshops, des conférences ou évènements.
Pour plus d’informations vous pouvez vous rendre sur le site du [design museum brussels]( https://designmuseum.brussels/).  

## La PLASTICotek & LAB 

Tout d’abord il faut savoir qu’il n’est pas possible de dresser une liste exhaustive des plastiques car il existe plusieurs mélanges possibles. Ce n’est jamais le même plastique entre un objet et un autre. 
La plasticotek reprend toutes les informations à savoir sur le plastique et les méthodes de productions les composants existants avec des exemples d’objets.   

![](../images/plasticotek.png)  

Pour comprendre les différents processus ainsi que l’origine du plastique Terry nous a conseillé de regarder la vidéo de l’émission [« C’est pas sorcier »](https://www.youtube.com/watch?v=irFEnEZhlNM&ab_channel=C%27estpassorcier)  


Dans les objets moulés on retrouve un point d’injection. On voit la dépouille du moule qui permet de reconnaitre le mode de fabrication. Aujourd’hui on cache le point d’injection pour que ce soit plus esthétique.  
 ![](../images/étagère.jpg) ![](../images/point_injection.jpg)

#### 3 familles de plastique :   

1)	**Thermoformable** : Quand on le chauffe il est possible d’**injecter** ou **extruder** le plastique. Ensuite lorsqu’il refroidit et durcit, il est possible ensuite de le re-chauffer et de le re-travailler. ->  **Opération réversible**.
2)	** Thermodurcissable** : Même principe sauf qu’on ne peut plus les re-travailler une fois refroidit. **Opération irréversible**. Par exemple, la Bakelite dont il n’est pas possible d’utiliser le procédé de l’injection car il faut une température trop élevée pour l’injecter. 
Il faut calculer que le plastique injecté est chaud et doit se refroidir de la même manière sur toutes les surfaces. Cela nécessite une étude de flux qui sert à la création du moule qui est l’élément le plus cher dans la réalisation de l’objet (centaine de millier d’euros). Il faut calculer la forme du moule et la vitesse d’injection pour que le plastique puisse s’étaler de la même façon de la même épaisseur et refroidir de la même façon partout.
**3 choses à prendre en compte : la température, la vitesse et la forme.** 
Certains objets sont rigidifiés en ajoutant des nervures lorsque l’épaisseur seule ne suffit. 
3)	**Elastomère** : ce sont les gommes

Tous les plastiques ont un acronyme qui permette de comprendre s’il s’agit d’un plastique thermoformable, Thermodurcissable ou Elastomère.
Chaque plastique sont produit différemment. Le procédé principal est l’**injection**.  

### Techniques de production :  

**Injection** : Le plastique utilisé se présente sous forme de billes placé dans un moule chauffé.   

![](../images/bille.gif)  

Ensuite, il est poussé à haute pression et haute température dans le moule fermé. Une fois que l’objet est formé le moule s’ouvre et parfois on ajoute des chariots et parfois par rapport à la forme il y a des parties qui s’enlèvent. Il est possible que l’injection se fait en plusieurs phase. Technique très complexe pensée par des ingénieurs spécialisés juste dans la conception de moules.   

![](../images/crétionmoule.gif)  

Le cycle de production doit être le plus court possible. Plus c’est rapide plus on comble la différence et les dépenses pour le moule.  

![](../images/injection__2_.jpg)   

![](../images/injection.gif) ![](../images/tracteur.JPG)    

Ici un [Guide des matières plastiques]( https://www.sagaertplastique.fr/guide-des-matieres-plastiques/) reprenant nom, abréviation, caractéristiques, usages des plastiques moulé avec injection. 
« Les matières les plus utilisées sont le PE, PP, PAS, PVC, PC, PET, POM, PA, PMMA, ABS »  


**Extrusion** : Technique plus simple et plus utilisé car moins couteuse. Utilisé dans la production de masse pour couvrir le prix du moule. Exemple de production : les tuyaux, les profilés.
![](../images/extrusion__2_.jpg)  


![](../images/extrusion.gif)  

Au lieu d’avoir un moule, on a juste une partie ou on pousse le plastique qui a une forme qui est le profil et le plastique est poussé et ensuite le bout est coupé.   

**Injection par soufflage** : Cette méthode se situe entre les deux techniques précédentes. Ici, on utilise principalement du PET. Par exemple : les bouteilles, avec un moule comme pour l’injection. Une partie (pipette ?) entre dans le plastique et souffle de l’air pour que le plastique gonfle et prenne la forme du moule. La machine est composée d’un ou trois bras qui tournent pour être plus rapide.  

![](../images/injectionsoufflage.gif)  

Avec le même procédé de soufflage par extrusion on fabrique les bouteilles de shampooing, de crème etc.
La quantité de plastique placé au bout de la pipette est calculé avec le moule pour que lors du soufflage la matière ait la bonne épaisseur. 
On distingue sur les bouteilles d’eau en plastique, en dessous, le point d’injection. Par contre, sur la bouteille de cosmétique on retrouve une ligne en dessous. Par exemple : sur une bouteille de savon vaisselle on retrouve une ligne et un cercle car c’est du soufflage par extrusion. On a le même procédé pour l’extrusion et ensuite on pince. Donc, il y a le plastique qui sort, on pince et on souffle.  

![](../images/IMG_1033.JPG)  

Tous les objets en production de masse sont souvent nés de ces trois procédés : extrusion, injection, et soufflage. 
Les sacs en plastique ont un procédé d’extrusion et de soufflage particulière pour obtenir une épaisseur très fine.   

**Rotomoulage** : On chauffe le moule qui contient des billes en plastique, le moule tourne de manière tridimensionnel grâce à une machine qui tourne sur son axe et dont chaque bras tournent aussi sur leurs axes pour répartir le plastique sur toute la surface du moule de la façon la plus homogène possible et on l’utilise pour faire de grand objet. (ex : les kayak). Ce type d’objet est toujours vide à l’intérieur. C’est aussi le procédé qui prend le plus longtemps ou on a la finition la moins bonne surtout le PE et le PP mais qui est la moins cher. La surface reste rugueuse, structurellement rigide et léger.
« Les matières les plus utilisées sont PE et le plastisol PVC, mais il est aussi possible de transformer du PP, du polycarbonate, des polyamides, ABS, acryliques, polyester, PS. »  

![](../images/roto.JPG)  


### KIT reconnaissance plastique  

Un **kit d’identification des plastiques** a été élaboré, il se compose d’**échantillon** qui permet de déterminé un plastique ou la famille à laquelle il appartient.  

![](../images/kit1.JPG) ![](../images/kit2.JPG)  


La boite reprend des échantillons de plastique qui va avec un **questionnaire de l’institut de Gand**. Les plastiques sont rangés et numérotés. Il faut se rendre sur le site d’[identification des plastiques](https://plastic.tool.cultureelerfgoed.nl/) et répondre à un questionnaire. On fait appel à trois sens : l’odorat, la vue et le touché. Il faut déterminer si c’est une mousse, un élastomère, un film ou si le plastique est rigide.
Par exemple, si c’est un plastique d’avant les années 50 on sait que ça ne peut pas être de l’ABS car il n’a pas été créé à ce moment-là. 
Le questionnaire procède par élimination et permet de se rapprocher le plus possible de la famille de de plastique qui pourrait correspondre avec l’échantillon.   

Test avec un objet plastique rigide, ici une ancienne radio.
Il est difficile de restaurer une pièce en plastique. La restauration peut être esthétique ou fait pour conserver l’objet en arrêtant les phénomènes de dégradation.  Ça dépend également de la nature du plastique. Tous les plastiques se dégradent mais de manière différente. Par exemple le PVC (ex : objet gonflable) il jaunit et les plastifiant du PVC vont migrer vers l’extérieur et va former des zones avec une espèce de liquide collant et visqueux qui va coller la poussière si l’objet n’est pas protégé
Il est possible de polir le plastique. Certains objets de type ethnographique (qui n’est pas un objet d’art). Soit on conserve les marques d’usures par le temps soit ça peut être un objet d’art que l’on veut restaurer à neuf et qu’on poli entièrement. Ne peut pas se faire sur tous les plastiques par exemple si le plastique est teinté dans la masse, en le polissant il restera de la même couleur mais certains objets dont la couleur et en surface vont perdre leur couleur.  

### La réserve   

L’espace reste toujours dans le **noir** pour une question de **conservation**. Pour la conservation les plastiques doivent être **séparés** car ils dégagent des **émanations**. 
La réserve contient tous les objets non exposés, elle est divisée par rayonnage ou ils séparent les plastiques. Certains plastiques ne peuvent pas rester l’un à côté de l’autre. Par exemple, le PVC et le PUR se trouve de l’autre côté de la réserve.    

![](../images/réserve.JPG)    

Pour la conservation tous les plastiques sauf le PVC (pour éviter qu’il ne colle car il transpire et colle) sont séparés avec une mousse.
_1er partie_ 
**ABS** ou Acrylonitrile butadiène styrène : plastique très utilisé dans les années 50-60 car la composition est basique et on pouvait faire tout et n’importe quoi. Défaut : le jaunissement et devient fragile avec le temps.   

Exemple ![](../images/jaunisement.JPG)    

Jaunissement dû à l’éclairage au néon après 6 mois. Le jaunissement apparait du au agents ignifuges bromés (retardateur de flamme). L’utilisation d’un produit appelé Retr0bright (composé de 'eau oxygénée à 10 % et de TAED) pourrait redonner la couleur d’origine du plastique jaunit.
Injecter ou extruder. Dessine la structure l’objet n’est pas produit par injection dans un moule mais injecter horizontalement et on coupe. Avec l’extrusion on reste avec des épaisseurs plus grosses que l’injection. Car les structure ne peuvent pas être complétement vide et on a besoin de nervures. A partir de l’objet on peut voir si c’est injecter (le petit cercle).
Certaines chaises possèdent des nervures en dessous toutes dans un sens, on ne peut pas avoir quelques choses d’horizontal. Par extrusion, on retrouve souvent des coins mais sinon c’est toujours un peu arrondi.
On utilisait aussi l’ABS pour tout ce qui était électroménager, radio dans les années 50-60. Cette matière est aussi utilisée pour faire des objets très petit et très détaillé.   

 ![](../images/ABS.JPG)    

**PVC** ou Polychlorure de vinyle: surtout utilisé pour les objets gonflables et est produit en film et ensuite electrosoudé ou thermosoudé. Ça peut être transparent, coloré, mate et peut être un revêtement. Il est aussi utilisé dans la fabrication de vinyle.   

![](../images/PVC.JPG)    

_2eme partie_ 
**PMMA** (plexiglass) ou Polyméthacrylate de méthyle : tout ce qui est transparent et produit en grande quantité qui n’a pas forcément des qualités optiques élevés. Le polycarbonate est meilleur en termes de qualité optique qui est plus brillant.   

![](../images/PMMA.JPG)    

_3eme partie_
**PP et PE** ou Polypropylène et Polyéthylène : couleur et objet léger, vide rotomoulé  

![](../images/PP__2_.JPG)  ![](../images/PE.JPG)  

`Petite note d’histoire`  

La couleur des objets est un indice sur type de plastique car il y eu différente palette de couleur à travers les décennies dans les années. Année 80 couleur un peu plus pastel, il fallait mobiliser le plastique. Les grandes entreprises ont changé leur marketing et change les couleurs et formes un peu plus organique qui se passe durant la période radicale, Memphis ou on cherche plus les couleurs primaires, les formes géométriques mais on cherche le kitch et la surprise, le post moderne.
La compagnie Alessi nait comme fonderie jusque dans les années 90 où il crée des objets en acier ou en aluminium ou inox. Avec la vague post moderne dans les années 90 il s’ouvre au plastique et ils font un workshop (FFF : Family Follows Fiction) dans lequel sont conçu une série de petit objet ou l’idée c’est que les gens ont marre de dire que la forme suit la fonction et il faut que le design soit communicatif et donc créer des compagnons de vie, des petits objets qui soit un rappel avec notre quotient. La communication de l’émotion et du rapport à l’objet devient importante appelé objet transactionnel c’est-à-dire que quand on voit cet objet il nous procure une émotion et qu’on a envie de l’avoir.
Dernière Canti lever de Vitra par Jasper Morrison réalisé en plastique chaise en porte à faux sorti en 2020. Chaise en une seule pièce et sans nervure. Ce modèle de chaise à évoluer au fil du temps.  

![](../images/cantilever_mart_stam.jpg)  S33 - acier chromé – cuir - 1925
![](../images/cantilever_vitra.jpg)  HAL - polypropylène teinté - tube d’acier chromé - 2010 
![](../images/cantiliver_jasper_morrison.JPG) EVO-C - polypropylène - 2020 

Phanton chair Assise relevé de 4 cm car les tables sont plus haute qu’avant. 
Thermoformage ; on part d’une feuille plastique avec une certaine épaisseur qu’on chauffe ensuite avec deux formes mâle et femelle qu’on emboite qui vient presser puis on aspire pour que la matière prenne forme. Les polymères les plus souvent utilisés sont : PS,PE,PP,PC,ABS,PVC,PMMA.  

![](../images/thermoformage__2_.jpg)    

Une autre manière de faire en utilisant l’aspiration comme montré ci-dessous.  

![](../images/thermoformage.gif)    

**PS** (Frigolite) ou polystyrène :  

A haute densité devient rigide. Permet de réaliser des objets rigide, léger et sans angle (bien pour les enfants).   

![](../images/frichair.JPG)  ![](../images/ingo.JPG)  

Le polystyrène est coulé à chaud ?  

`Petite note d’histoire`  

Kartell : Giulio Castelli élève de Giulio Natta inventeur du polypropylène. Au début Kartell vendait des élastiques de porte ski pour la voiture et après ils ont travaillé le plastique.  Une génération a étudié comment on pouvait créer des objets pour le quotidien. Ensuite il y a eu la partie postmodernisme il y a eu les formes et les matériaux qui ont changé. Les derniers départements qui ont vu le jour se consacre aux enfants et à la lumière. 
Polypropylène un peu comme le polyéthylène : objet rotomoulé avec finition rugueuse et souvent de grand objet. Pas lisse comme l’ABS   

![](../images/objetroto.JPG) ![](../images/finition.JPG) ![](../images/texture.JPG) 

Cache point d’injection   

![](../images/cache_.JPG)   

_4eme partie_
**GRP** ou Glass Reinforced Plastic : fibre de verre lisse à l’extérieur et rugueux à l’intérieur (on voit les fibres).  

![](../images/grp.JPG) ![](../images/lissse.JPG) ![](../images/rugueux.JPG) 

La technique avec la fibre de verre était utilisée dans la confection de bateau 
PC plus brillant que le PMMA   

_5 eme partie_ 
Résine 
**Bakelite** : surtout pour des parties techniques ou pour des objets comme des rasoir ou bijoux. On essaye de reproduire des matériaux nobles comme de l’ivoire, de la tortue, etc. car à l’époque c’est cher. C’est un plastique thermodurcissable (irréversible) et lourd  

![](../images/bakelite.JPG) 

**Melamine** : plastique avant l’ABS 

_6eme partie_   
**PUR** ou Polyuréthane : Mousse, très fragile et vieillisse craque et s’effrite   

`Petite note d’histoire`  

Cactus radical design de Gufram (italien) qui crée un vernis Guflac qui recouvre les mousses (photo cactus).   

![](../images/mou.JPG)   

La première chaise avec du rembourrage « Lady » de Marco Zanuso apparait en 51 à Milan. Mais elle possédait encore une structure en bois.   

 ![](../images/lady.jpg)   

Par la suite ils sont parvenus avec le plastique à faire une structure autoportante avec que de la mousse. Au niveau du cout et du temps de production cela représente une grande avancé technique.   

![](../images/mousseux.JPG) 


