# Semaine 2   


En 2014 l’Atomium va acheter la collection de plasticarium de Philippe Decelle. C'est un ingénieur artiste qui amoureux du PMMA et qui va décider à collectionner des objets de design, mobilier, objets du quotidien et des œuvres d’art que en plastique  

 ![](../images/theplasticcollection.JPG)   

3 critères lancent sa collection : 
1)	Amoureux du PMMA et est déjà un artiste il crée des tableaux en plexiglass
2)	Acheté de Garden Egg de Peter Ghyczy 
3)	En se promenant dans les rues de Uccle il trouve la chaise de Joe colombo.   

![](../images/chaisPD.JPG)   

Là il commence sa collection et décide de collectionner uniquement en plastique. Cela et lié à l’histoire que les gens commencent à jeter et ne font pas attention. Il décide donc d’ouvrir une galerie au centre-ville qui s’appelle le plasticarium. Pourquoi ce nom ? car un musée des planètes c’est un planétarium et un musée de Plastique c’est un plasticarium.
Il collectionne jusque début des années 2010 et puis va vouloir vendre sa collection. Sa collection sera rachetée par l’Atomium en 2014. Un an après le musée ouvre. 1er expo de ses pièces avant l’achat aura lieu dans l’Atomium.  

![](../images/IMG_1044.JPG) ![](../images/IMG_1045.JPG) ![](../images/IMG_1046.JPG) ![](../images/IMG_1047.JPG) ![](../images/IMG_1048.JPG)   

Il exposera également ses pièces à la fondation pour l’architecture (CIVA). D’autres musée comme le centre Pompidou et le MOMA voulaient acheter les pièces mais l’idée était de pas disperser un patrimoine belge en tant que collection.
L’exposition n’est pas totalement dans l’ordre chronologique ni classé par thématique.
Plus on avance dans la visite plus ça devient chronologique. 
Le but à travers les différentes thématiques c’est de montrer l’impact des plastiques dans la création design mais aussi l’impact des plastiques dans la vie quotidienne. Le changement historique avec le boom de plastique, les baby-boomers à l'école sixties.
La première salle est accentuée sur le rêve américain. On retrouve la production industrielle avec le boom des plastiques en 50 -60 donnant suite à la crise de 1929. Aux États-Unis ils avaient l'idée de revigorer, de réhabiliter, de revitaliser l'environnement américain. Petit à petit des figures du design sont intervenus pour pallier cette revitalisation et donc de remeubler les intérieurs, créer de nouveaux objet, etc.. C’est également une période où les avancés technologique commence à arriver donc permet de créer de nouveaux objets. Le plastique vient aider à de nouvelle création pour que les avancés technologiques puissent être insérées dans des boitier en plastique.
Après la 2nde guerre mondiale apparait des figures majeures qui sont aussi des entreprises comme Knoll, Herman Miller, Vitra qui participent à la diffusion du made in usa. Donc toutes les avancés technologique des USA vont pouvoir être diffusé en Europe grâce au plan Marshall.
Raymond Loewy designer français qui travaille au USA  il réalise des logos connus comme Lucky strike, Shell, etc. C’est également un des 1 er designer qui fait la couverture du TIME magazine. Ici, message : seconde guerre mondial- réhabiliter la nation - créer des objets.  

![](../images/IMG_1052.JPG)   

Herman Miller dont les objets vont être récupéré par vitra. 
Tupperware possède une succursale en Belgique ainsi que Knoll tout le Made in usa vient inonder le marché européen.    

![](../images/IMG_1060.JPG)   

Designer italien travail en Italie et aux USA, Lella Vignelli et qui refont le plan du métro ou propose nouveau logo pour Knoll. Il y a un échange de designer.  

![](../images/IMG_1062.JPG)   

Au même moment profusion d’objet qui viennent des états unis et aussi le boom du plastique année 50. Changement de consommation et de production dans la société. Les plastiques aident énormément car c’est un matériau pas cher. Ce qui coûte cher c’est le moule pour le rentabiliser il faut produire en masse. Les plastiques qui viennent comme aide pour une nouvelle esthétique. La matière est très modulable donc  les avancés techniques permettent des choses comme des télés portatives, de réduire la tailler des objets, d’intégrer l’ensemble des procédés techniques à l’intérieur d’une coque en plastique et d’avoir une autre esthétique.   

![](../images/IMG_1069.JPG)   

Nous sommes aussi dans un changement en termes de personne, au moment des golden sixties plus tard. Ils veulent se défaire des carcans familiaux donc se défaire des gros meubles en bois ou on encastre la télé dedans, des meubles avec lesquels on ne peut pas voyager on est dans une ère qui arrive avec mai 68. Ici cette envie de voyager être plus libre, les objets en plastiques viendront répondre aux demandes de cette nouvelle génération. Par exemple la machine  à écrire en métal devient en plastique transportable. La technique est cachée dans la boite ce qui était impressionnant dans les années 60.  

![](../images/IMG_1070.JPG)   

Nouvelle chose qui arrive c’est qu’il est possible d’avoir une profusion de couleur en injectant du pigment dans la matière. Permet à n’importe qui de s’acheter du mobilier à des prix abordable. Un autre avantage c’est qu’il n’y a pas d’angle droit.  

![](../images/IMG_1067.JPG)   


En Europe une grande firme participe à la production, Kartell, qui est une industrie italienne qui va participer à diffuser du mobilier en plastique. C’est des pionniers en termes de recherche plastique. Il travaille dans les débuts majoritairement avec des designers italiens pour offrir une gamme d’objet de toutes les couleurs et de participer à cette consommation et production de masse. Les évènements historiques sont liés aux évènements artistiques.
1er salle : mise en avant sur la production de masse, société de consommation, baby-boomers.
Mobiliers éphémères facilement remplaçable. A l’époque le plastique n’était pas cher mais avec l’industrie pétrolière les prix aujourd’hui ont augmenté à cela s’ajoute la valeur historique des objets.
2eme salle : montre la plasticité du plastique montre qu’est ce qui est possible de faire avec la matière. 1er salle plutôt produits « standards ». 
Réalisation de mobilier en forme de dent par le designer Wendell Castle.  

![](../images/IMG_1080.JPG)   

Designer Gunter Belzig joue sur les formes de la nature. Chaise dont le dossier supporte la nuque et les reins qui s’utilise à l’intérieur comme à l’extérieur. Rainures au centre qui permet à l’eau de s’écouler vers l’extérieur lorsqu’il pleut.  

![](../images/IMG_1082.JPG)   


Podium de design anthropomorphe. 
La chaise a été moulé sur un homme et a été produit en série. On parle toujours de la femme objet, ici Ruth Francken transforme le corps de l’homme en un objet sur lequel on peut s’asseoir.   

![](../images/IMG_1092.JPG)   

Thématique du porte à faux.  

![](../images/IMG_1087.JPG)   

Phantom crée la première chaise en porte a faux moulée en un seul tenant et en plastique. Puis à travailler avec vitra. Le podium montre l’évolution de l’ensemble des chaises en porte à faux. Procédé technique intéressant car le but est de créé une chaise sans pieds arrière et ou la tension est mise sur l’avant de l’assise.
3eme salle toujours année 60 montrer comment l’impact de la science-fiction et des premiers pas sur la lune vont influencer les designers. Les designers travaillent les forment et répondent à ce qui se passe de manière historique. Par exemple Phanton lors de l’exposition visionna va créer des univers totaux. Du sol au plafond tout est fait en plastique.  

![](../images/IMG_1095.JPG) 

Dondolo de Leonardi et Stagi avec un aspect futuriste. Elle donne l’impression qu’on est en apesanteur lorsqu’on s’assis dessus.  

![](../images/IMG_1094.JPG)   


Maurice Calka designer français réalise le bureau boomerang et PDG.  Un jour Calka à dessiner pour un des président de l’Élysée. Chaque président français demande à un designer français de faire le mobilier de son bureau.  

 ![](../images/IMG_1096.JPG)   

Nouvelle caractéristique : la transparence avec l’idée d’avoi des objets plus léger visuellement. Avec des pigments il y a possibilité d’avoir des objet transparent et coloré. Ici, réuni en thématique pour montrer cette caractéristique propre au plastique mais pourrait être ailleurs, correspond à la production de masse.  

![](../images/IMG_1098.JPG)   

Autre caractéristique : les gonflables sont des feuilles de PVC soudées entre elles. On est dans une société ou les nouvelles générations aiment voyager sont dans une optique d’avoir du mobilier « éphémère ». Réponse des designers est de créer du mobilier en gonflable qui est facilement transportable.   

![](../images/IMG_1099.JPG)   


Radical design
Courant qui se développe plus en architecture qu’en design. Les designers profitent de la matière pour s’amuser. 1er branche il crée mobilier et objet en utilisant la mousse (polyuréthane). Jamais de la grande production ils entrent en contradiction ne voulant pas créer en masse mais tout en utilisant du plastique. Esthétique particulière.  Objet détourné de leur fonction.   

 ![](../images/IMG_1100.JPG)   

Au début Philippe Decelle ne collectionne pas uniquement des éléments de design mais aussi de l’art. il y a donc une exposition dédiée à un cabinet de curiosité aux parties des œuvres d’art que Philippe a collectionné.  

![](../images/IMG_1102.JPG) ![](../images/IMG_1104.JPG)   

POST MODERNISME  

Deux grands mouvements : Memphis et ??
L’idée derrière le post modernisme c’est de revenir à des sentiments dans les  objets car ils trouvent que les objets sont plus liés aux gens et donc ils veulent recréer du lien entre utilisateur et les objets. Changement aussi au niveau des couleurs plus pastel. Utilisation d’ancien ici, le trône revisité ou la chaise Proust, fauteuil louis XIV moulé en plastique d’Alexandre Mendini.  

![](../images/IMG_1106.JPG)   

Podium année 90 avec grande entreprise comme Kartell qui s’internationalise au départ travaille que avec des designer italien et elle ouvre les portes à des designers et faire des collaborations comme Kartell et Starck. Les entreprises vont vouloir redorer leur image et donc faire appel a de nouveau designer pas forcément italien pour créer de nouveau objet avec une nouvelle esthétique.   

![](../images/IMG_1111.JPG)   


Exemple : le cheval à bascule, inspiré d’une poutre profilée H.
Les années 90 nouveau courant  influencé par l’art minimal.  Les objets sont plus fins avec un esthétique harmonieux.  

![](../images/IMG_1112.JPG)   

Développement des nouvelles technologies. 
1er ordinateur IMac transparent, lien minimalisme et hightech tout ce qui est technique qui est normalement caché est mis en avant, le mécanisme devient ornement.  

![](../images/IMG_1118.JPG)   

Impression 3D  

![](../images/IMG_1120.JPG)   

Vaisselle avec texture imitation cristal qui est en réalité du plastique.  

![](../images/IMG_1119.JPG)   

Fauteuil Starck 1 er fauteuil moulé en un seul tenant creux à l’intérieur.  

![](../images/IMG_1121.JPG)   

Face à cette production de masse, le boom des plastiques la crise pétrolière 1973. Pollution on ne pas passer outre la question des plastiques recyclé face au objet a usage unique. Alain gilles fauteuil en plastique recyclé.  

![](../images/IMG_1123.JPG)   

Chaise en aluminium d’Emeco produite pour navire et sous-marin de la NAVY. Collaboration avec coca-cola, recycle 11 bouteilles pour créer une chaise NAVY.  

![](../images/IMG_1126.JPG)   

Nouvelle vague de design lié à l’artisanat pour recherche technique seront plutôt illustrée par gravity stool mélange émaille de fer avec du plastique  qui est tiré par un aimant de manière artisanal. Hors-piste duo de designer français travaille avec communauté du Burkina Faso. Personne récupère caoutchouc des pneus. Pas du design de production mais de recherche.  

![](../images/IMG_1129.JPG)   

Dialo designer africain, structure en métal entouré de fil.  

![](../images/IMG_1130.JPG)   

Chaise imprimé en 3D à base de frigos.  

![](../images/IMG_1132.JPG ) 
