# Semaine 4    

# ![](../images/decro.png) ![](../images/steam.JPG)  

Nous avons débuté la journée avec la rencontre de Yan, professeur à l’école Decroly connue pour sa méthode de pédagogie active, Steph et Flo membre du Steamlab. Le but de la journée était d’organiser des activités avec les enfants de la classe de Yan.
Nous avons enchainé avec une visite du musée avec le module 3 en tant que guide. Après cela nous avons debriefer celle-ci dans la plasticothèque.
Différents points ont été soulevé :   

-	Manque d’un fil conducteur
-	Manque de chronologie, besoin de date pour se repérer
-	Temps d’attention des enfants est plus courts donc il faut adapter la visite
-	Modeler l’histoire pour l’adapter aux enfants
-	Nécessaire de sélectionner des pièces qui résume un ensemble (prendre l’idée principale)
-	Faire des liens entre objet, histoire et élément concret
-	Essayer de surprendre pour capter l’attention
-	Se concentrer sur des anecdotes parlantes
-	Les enfants doivent être acteur et actif
-	Atelier bricolage serait intéressant  

Ensuite nous avons fait un  brainstorming sur les activités qu’on pourrait faire avec les enfants de la classe de Yan avant l’arrivé au musée et pour le jour de leur venu. Il fallait réfléchir à comment adapter la visite aux enfants. Nous avons également fait une sélection des objets les plus parlants.  

Liste des objets sélectionnés :  

1)	Caillou et herbe de Gufram
2)	Plateau de Gaetano Petche
3)	Fauteuil gonflable de kha
4)	Ghost chair
5)	Sphere isolement
6)	Dondolo Cesare et Stagi
7)	Panton
8)	Dent Wendell Castle
9)	Vêtement, textile
10)	Fauteuil half and half
11)	Chaise homme de Ruth Vrancken
12)	Télé
