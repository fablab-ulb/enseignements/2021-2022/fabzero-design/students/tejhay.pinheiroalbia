## A PROPOS DE MOI  

![](images/compress2.jpg)
  
  
Salut !  
Je m’appelle **Tejhay** je suis actuellement en **master 2** à l’ULB la cambre Horta architecture. 
  
## A PROPOS DE MA VIE
Je suis né et j’ai grandi en Belgique dans la commune de Saint-Gilles, connu pour être un quartier multiculturel et artistique. Je me suis toujours intéressé de près ou de loin au design. Mon intérêt pour les arts et la création vient surement du trajet pour aller à l’école que j’ai effectué pendant une quinzaine d’année de la crèche à ma rétho. En effet, je passais par la rue Haute à l’aller et par la rue Blaes au retour, deux rues  remplies d’antiquaires et de galeries d’artistes, ce fut pour moi assez inspirant. 
Ayant toujours suivi des études dans l'enseignement général,je n'avais pas la possibilité de m'épanouir pleinement. C'est seulement au moment de choisir mes études universitaire que l'architecture s'est imposé à moi comme une évidence.
J’ai donc fait mes années de bachelier en architecture à l’ULB.




Si tu veux voir mes travaux, voilà mon GIT de l'année passée [GIT 2020-2021](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/tejhay.pinheiroalbia/-/tree/master). 


![](IMG_0839.JPG)
